// SPDX-License-Identifier: GPL-2.0
/*
 * Rockchip Video Decoder HEVC backend
 *
 * Copyright (C) 2022 Collabora, Ltd.
 *      Sebastian Fricke <sebastian.fricke@collabora.com>
 *
 * Copyright (C) 2019 Collabora, Ltd.
 *	Boris Brezillon <boris.brezillon@collabora.com>
 *
 * Copyright (C) 2016 Rockchip Electronics Co., Ltd.
 *	Jeffy Chen <jeffy.chen@rock-chips.com>
 */

#include <media/v4l2-mem2mem.h>
#include <linux/bitops.h>

#include "rkvdec.h"
#include "rkvdec-regs.h"
#include "rkvdec-hevc-data.c"

static int debug_dump;
module_param(debug_dump, int, 0644);

#define DUMP_REGISTER			0x1
#define DUMP_PPS			0x2
#define DUMP_RPS			0x4
#define DUMP_SL				0x8

#define dump_values(level, fmt, arg...)					\
	do {								\
		if (debug_dump & (level))				\
			pr_warn("RKVDEC HEVC: " fmt, ## arg);		\
	} while (0)

static void dump_registers(struct rkvdec_dev *rkvdec)
{
	print_hex_dump(KERN_INFO, "rkvdec_hevc: ", DUMP_PREFIX_OFFSET, 32, 4,
		       (__force const void *)rkvdec->regs, 320, false);
}

/* Size in u8/u32 units. */
#define RKV_SCALING_LIST_SIZE		1360
#define SCALING_LIST_SIZE_NUM		4
#define RKV_PPS_SIZE			(80 / 4)
#define RKV_PPS_LEN			64
#define RKV_RPS_SIZE			(32 / 4)
#define RKV_RPS_LEN			600
#define RKV_PPS_MAX_COLUMNS		20
#define RKV_PPS_MAX_ROWS		22
#define RKV_RPS_MAX_REFERENCES		15
#define RKV_MAX_DEPTH_IN_BYTES		2

/*
 * Description of the PPS memory blob, as expected by the hardware, as a bitfield.
 * mbz = must be zero, mbo = must be one, these are paddings that were copied from
 * the MPP implementation.
 */
struct rkvdec_sps_pps_bit_field {
	/* SPS */
	u8 video_parameter_set_id: 4;
	u8 seq_parameter_set_id: 4;
	u8 chroma_format_idc: 2;
	u16 pic_width_in_luma_samples: 13;
	u16 pic_height_in_luma_samples: 13;
	/* 36 bits above - index 0 */
	u8 bit_depth_luma: 4;
	u8 bit_depth_chroma: 4;
	/* GCC complains for some values when the unit is u8 even though the bit
	 * size is smaller than 8 */
	u16 log2_max_pic_order_cnt_lsb: 5;
	u8 log2_diff_max_min_luma_coding_block_size: 2;
	u8 log2_min_luma_coding_block_size: 3;
	u16 log2_min_transform_block_size: 3;
	u8 log2_diff_max_min_luma_transform_block_size: 2;
	u8 max_transform_hierarchy_depth_inter: 3;
	u16 max_transform_hierarchy_depth_intra: 3;
	/* 65 bits above - index 1 */
	u8 scaling_list_enabled_flag: 1;
	u8 amp_enabled_flag: 1;
	u8 sample_adaptive_offset_enabled_flag: 1;
	u8 pcm_enabled_flag: 1;
	u16 pcm_sample_bit_depth_luma: 4;
	u8 pcm_sample_bit_depth_chroma: 4;
	u8 pcm_loop_filter_disabled_flag: 1;
	u16 log2_diff_max_min_pcm_luma_coding_block_size: 3;
	u8 log2_min_pcm_luma_coding_block_size: 3;
	u16 num_short_term_ref_pic_sets: 7;
	u8 long_term_ref_pics_present_flag: 1;
	u16 num_long_term_ref_pics_sps: 6;
	/* 100 bits above - index 2 */
	u8 sps_temporal_mvp_enabled_flag: 1;
	u8 strong_intra_smoothing_enabled_flag: 1;
	u16 _mbz_1: 7;
	u32 _mbo_1: 21;
	/* 130 bits above - index 3 */
	/* PPS */
	u8 pic_parameter_set_id: 6;
	u16 pps_seq_parameter_set_id: 4;
	u8 dependent_slice_segments_enabled_flag: 1;
	u8 output_flag_present_flag: 1;
	u16 num_extra_slice_header_bits: 13;
	u8 sign_data_hiding_enabled_flag: 1;
	u8 cabac_init_present_flag: 1;
	u8 num_ref_idx_l0_default_active: 4;
	/* 161 bits above - index 4 */
	u16 num_ref_idx_l1_default_active: 4;
	u16 init_qp_minus26: 7;
	u8 constrained_intra_pred_flag: 1;
	u8 transform_skip_enabled_flag: 1;
	u8 cu_qp_delta_enabled_flag: 1;
	u8 log2_min_cu_qp_delta_size: 3;
	u8 pps_cb_qp_offset: 5;
	u16 pps_cr_qp_offset: 5;
	u8 pps_slice_chroma_qp_offsets_present_flag: 1;
	u8 weighted_pred_flag: 1;
	u8 weighted_bipred_flag: 1;
	u8 transquant_bypass_enabled_flag: 1;
	/* 192 bits above - index 5 */
	u8 tiles_enabled_flag: 1;
	u8 entropy_coding_sync_enabled_flag: 1;
	u8 loop_filter_across_slices_enabled_flag: 1;
	u8 loop_filter_across_tiles_enabled_flag: 1;
	u8 deblocking_filter_override_enabled_flag: 1;
	u8 deblocking_filter_disabled_flag: 1;
	u8 pps_beta_offset_div2: 4;
	u8 pps_tc_offset_div2: 4;
	u8 lists_modification_present_flag: 1;
	u8 log2_parallel_merge_level: 3;
	u8 slice_segment_header_extension_present_flag: 1;
	u8 _mbz_2: 3;
	u16 num_tile_columns: 5;
	u8 num_tile_rows: 5;
	/* 224 bits above - index 6 */
	u8 pps_m_mode: 2;
	u64 _mbo_2: 30;
	/* 256 bits above - index 7 */
	u8 column_width[20];
	/* 416 bits above - indeces 9, 10, 11, 12, 13 */
	u8 row_height[22];
	/* 592 bits above - indeces 14, 15, 16, 17, 18 & first half 19 */
	u32 scaling_list_address: 32;
	u16 _mbo_3: 16;
	/* 640 bits above - second half of index 19 & index 20 */
} __packed;

struct rkvdec_sps_pps_packet {
	u32 info[RKV_PPS_SIZE];
};

struct rkvdec_rps_packet {
	u32 info[RKV_RPS_SIZE];
};

/* Data structure describing auxiliary buffer format. */
struct rkvdec_hevc_priv_tbl {
	u8 cabac_table[RKV_CABAC_TABLE_SIZE];
	u8 scaling_list[RKV_SCALING_LIST_SIZE];
	struct rkvdec_sps_pps_packet param_set[RKV_PPS_LEN];
	struct rkvdec_rps_packet rps[RKV_RPS_LEN];
};

struct rkvdec_hevc_run {
	struct rkvdec_run base;
	const struct v4l2_ctrl_hevc_slice_params *slices_params;
	const struct v4l2_ctrl_hevc_decode_params *decode_params;
	const struct v4l2_ctrl_hevc_sps *sps;
	const struct v4l2_ctrl_hevc_pps *pps;
	const struct v4l2_ctrl_hevc_scaling_matrix *scaling_matrix;
	int num_slices;
};

struct rkvdec_hevc_ctx {
	struct rkvdec_aux_buf priv_tbl;
	struct v4l2_ctrl_hevc_scaling_matrix scaling_matrix_cache;
};

struct scaling_factor {
	u8 scalingfactor0[1248];
	u8 scalingfactor1[96];	 /*4X4 TU Rotate, total 16X4*/
	u8 scalingdc[12];		  /*N1005 Vienna Meeting*/
	u8 reserverd[4];		   /*16Bytes align*/
};

struct bit_writer_cursor {
	/* the current byte */
	u8 *b;
	/* the current bit within *b, nomalized: 0..7 */
	u8 bit;
};

struct bit_writer {
	struct bit_writer_cursor cur;
	u32 *buf;
	u32 buf_len;		/* in bytes */
};

static inline void cursor_reset(struct bit_writer_cursor *cur, void *s)
{
	cur->b = s;
	cur->bit = 0;
}

static inline void cursor_advance(struct bit_writer_cursor *cur, unsigned int bits)
{
	bits += cur->bit;
	cur->b = cur->b + (bits >> 3);
	cur->bit = bits & 7;
}

void bit_writer_init(struct bit_writer *bw, u32 *s, u32 len)
{
	bw->buf = s;
	bw->buf_len = len;
	cursor_reset(&bw->cur, bw->buf);
}

void bit_writer_put_bits(struct bit_writer *bw, u32 value, u8 length)
{
	u8 *b = bw->cur.b;
	unsigned int tmp;

	if (length == 0)
		return;

	/* paranoia: strip off hi bits; they should not be set anyways. */
	if (length < 32) {
		value &= ~0ULL >> (32 - length);
	}

	*b++ |= (value & 0xff) << bw->cur.bit;

	for (tmp = 8 - bw->cur.bit; tmp < length; tmp += 8) {
		*b++ |= (value >> tmp) & 0xff;
	}

	cursor_advance(&bw->cur, length);
}

static void translate_pps(struct rkvdec_hevc_run *run, struct rkvdec_sps_pps_bit_field *output, dma_addr_t dma_base_addr)
{
	const struct v4l2_ctrl_hevc_sps *sps = run->sps;
	const struct v4l2_ctrl_hevc_pps *pps = run->pps;
	u8 pcm_enabled = 0;
	u32 min_cb_log2_size_y, ctb_log2_size_y, ctb_size_y, log2_min_cu_qp_delta_size;
	u32 scaling_distance, width, height, max_min_cb_size, x, pic_in_cts_width, pic_in_cts_height;
	u32 tmp_w = 0;
	u32 prev_w = 0;
	u32 tmp_h = 0;
	u32 prev_h = 0;
	dma_addr_t scaling_list_address;
	u16 column_width[20] = {0};
	u16 row_height[22] = {0};
	int i;

	min_cb_log2_size_y = sps->log2_min_luma_coding_block_size_minus3 + 3;
	width = sps->pic_width_in_luma_samples;
	height = sps->pic_height_in_luma_samples;
	max_min_cb_size = sps->log2_diff_max_min_luma_coding_block_size;

	/* SPS data */
	output->video_parameter_set_id = sps->video_parameter_set_id;
	output->seq_parameter_set_id = sps->seq_parameter_set_id;
	output->chroma_format_idc = 1;
	output->pic_width_in_luma_samples = sps->pic_width_in_luma_samples;
	output->pic_height_in_luma_samples = sps->pic_height_in_luma_samples;
	output->bit_depth_luma = sps->bit_depth_luma_minus8 + 8;
	output->bit_depth_chroma = sps->bit_depth_chroma_minus8 + 8;
	output->log2_max_pic_order_cnt_lsb = sps->log2_max_pic_order_cnt_lsb_minus4 + 4;
	output->log2_diff_max_min_luma_coding_block_size = sps->log2_diff_max_min_luma_coding_block_size;
	output->log2_min_luma_coding_block_size = sps->log2_min_luma_coding_block_size_minus3 + 3;
	output->log2_min_transform_block_size = sps->log2_min_luma_transform_block_size_minus2 + 2;
	output->log2_diff_max_min_luma_transform_block_size = sps->log2_diff_max_min_luma_transform_block_size;
	output->max_transform_hierarchy_depth_inter = sps->max_transform_hierarchy_depth_inter;
	output->max_transform_hierarchy_depth_intra = sps->max_transform_hierarchy_depth_intra;
	output->scaling_list_enabled_flag = !!(sps->flags & V4L2_HEVC_SPS_FLAG_SCALING_LIST_ENABLED);
	output->amp_enabled_flag = !!(sps->flags & V4L2_HEVC_SPS_FLAG_AMP_ENABLED);
	output->sample_adaptive_offset_enabled_flag = !!(sps->flags & V4L2_HEVC_SPS_FLAG_SAMPLE_ADAPTIVE_OFFSET);
	pcm_enabled = !!(sps->flags & V4L2_HEVC_SPS_FLAG_PCM_ENABLED);
	output->pcm_enabled_flag = pcm_enabled;
	output->pcm_sample_bit_depth_luma = pcm_enabled ? sps->pcm_sample_bit_depth_luma_minus1 + 1 : 0;
	output->pcm_sample_bit_depth_chroma = pcm_enabled ? sps->pcm_sample_bit_depth_chroma_minus1 + 1 : 0;
	output->pcm_loop_filter_disabled_flag = !!(sps->flags & V4L2_HEVC_SPS_FLAG_PCM_LOOP_FILTER_DISABLED);
	output->log2_diff_max_min_pcm_luma_coding_block_size = sps->log2_diff_max_min_pcm_luma_coding_block_size;
	output->log2_min_pcm_luma_coding_block_size = pcm_enabled ? sps->log2_min_pcm_luma_coding_block_size_minus3 + 3 : 0;
	output->num_short_term_ref_pic_sets = sps->num_short_term_ref_pic_sets;
	output->long_term_ref_pics_present_flag = !!(sps->flags & V4L2_HEVC_SPS_FLAG_LONG_TERM_REF_PICS_PRESENT);
	output->num_long_term_ref_pics_sps = sps->num_long_term_ref_pics_sps;
	output->sps_temporal_mvp_enabled_flag = !!(sps->flags & V4L2_HEVC_SPS_FLAG_SPS_TEMPORAL_MVP_ENABLED);
	output->strong_intra_smoothing_enabled_flag = !!(sps->flags & V4L2_HEVC_SPS_FLAG_STRONG_INTRA_SMOOTHING_ENABLED);
	/* Padding */
	output->_mbz_1 = 0;
	output->_mbo_1 = 0x1fffff;

	/* PPS data */
	output->pic_parameter_set_id = pps->pic_parameter_set_id;
	output->seq_parameter_set_id = sps->seq_parameter_set_id;
	output->dependent_slice_segments_enabled_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_DEPENDENT_SLICE_SEGMENT_ENABLED);
	output->output_flag_present_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_OUTPUT_FLAG_PRESENT);
	output->num_extra_slice_header_bits = pps->num_extra_slice_header_bits;
	output->sign_data_hiding_enabled_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_SIGN_DATA_HIDING_ENABLED);
	output->cabac_init_present_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_CABAC_INIT_PRESENT);
	output->num_ref_idx_l0_default_active = pps->num_ref_idx_l0_default_active_minus1 + 1;
	output->num_ref_idx_l1_default_active = pps->num_ref_idx_l1_default_active_minus1 + 1;
	output->init_qp_minus26 = pps->init_qp_minus26;
	output->constrained_intra_pred_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_CONSTRAINED_INTRA_PRED);
	output->transform_skip_enabled_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_TRANSFORM_SKIP_ENABLED);
	output->cu_qp_delta_enabled_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_CU_QP_DELTA_ENABLED);

	ctb_log2_size_y = min_cb_log2_size_y +
		sps->log2_diff_max_min_luma_coding_block_size;
	ctb_size_y = 1 << ctb_log2_size_y;
	log2_min_cu_qp_delta_size = ctb_log2_size_y - pps->diff_cu_qp_delta_depth;
	output->log2_min_cu_qp_delta_size = log2_min_cu_qp_delta_size;
	output->pps_cb_qp_offset = pps->pps_cb_qp_offset;
	output->pps_cr_qp_offset = pps->pps_cr_qp_offset;
	output->pps_slice_chroma_qp_offsets_present_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_PPS_SLICE_CHROMA_QP_OFFSETS_PRESENT);
	output->weighted_pred_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_WEIGHTED_PRED);
	output->weighted_bipred_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_WEIGHTED_BIPRED);
	output->transquant_bypass_enabled_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_TRANSQUANT_BYPASS_ENABLED);
	output->tiles_enabled_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_TILES_ENABLED);
	output->entropy_coding_sync_enabled_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_ENTROPY_CODING_SYNC_ENABLED);
	output->loop_filter_across_slices_enabled_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_PPS_LOOP_FILTER_ACROSS_SLICES_ENABLED);
	output->loop_filter_across_tiles_enabled_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_LOOP_FILTER_ACROSS_TILES_ENABLED);
	output->deblocking_filter_override_enabled_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_DEBLOCKING_FILTER_OVERRIDE_ENABLED);
	output->deblocking_filter_disabled_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_PPS_DISABLE_DEBLOCKING_FILTER);
	output->pps_beta_offset_div2 = pps->pps_beta_offset_div2;
	output->pps_tc_offset_div2 = pps->pps_tc_offset_div2;
	output->lists_modification_present_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_LISTS_MODIFICATION_PRESENT);
	output->log2_parallel_merge_level = pps->log2_parallel_merge_level_minus2 + 2;
	/* MPP demands that this should always be 0 ?!? */
	output->slice_segment_header_extension_present_flag = !!(pps->flags & V4L2_HEVC_PPS_FLAG_SLICE_SEGMENT_HEADER_EXTENSION_PRESENT);
	output->_mbz_2 = 0;
	output->num_tile_columns = pps->num_tile_columns_minus1 + 1;
	output->num_tile_rows = pps->num_tile_rows_minus1 + 1;
	/* FIXME: Describe this hard coded value from MPP properly */
	output->pps_m_mode = 3;
	output->_mbo_2 = 0x3fffffff;

	if (pps->flags & V4L2_HEVC_PPS_FLAG_TILES_ENABLED) {
		if (pps->flags & V4L2_HEVC_PPS_FLAG_UNIFORM_SPACING) {
			x = 1 << (min_cb_log2_size_y + max_min_cb_size);
			pic_in_cts_width = (width + x - 1) / (x);
			pic_in_cts_height = (height + x - 1) / (x);
			for (i = 0; i <= pps->num_tile_columns_minus1; i++) {
				tmp_w = ((i + 1) * pic_in_cts_width) / (pps->num_tile_columns_minus1 + 1);
				column_width[i] = tmp_w - prev_w - 1;
				prev_w = tmp_w;
			}
			for (i = 0; i <= pps->num_tile_rows_minus1; i++) {
				tmp_h = ((i + 1) * pic_in_cts_height) / (pps->num_tile_rows_minus1 + 1);
				row_height[i] = tmp_h - prev_h - 1;
				prev_h = tmp_h;
			}
		} else {
			for (i = 0; i <= pps->num_tile_columns_minus1; i++)
				column_width[i] = pps->column_width_minus1[i];
			for (i = 0; i <= pps->num_tile_rows_minus1; i++)
				row_height[i] = pps->row_height_minus1[i];
		}
	} else {
		column_width[0] = ((sps->pic_width_in_luma_samples + ctb_size_y - 1) / ctb_size_y) - 1;
		row_height[0] = ((sps->pic_height_in_luma_samples + ctb_size_y - 1) / ctb_size_y) - 1;
	}

	for (i = 0; i < RKV_PPS_MAX_COLUMNS; i++)
		output->column_width[i] = column_width[i];
	for (i = 0; i < RKV_PPS_MAX_ROWS; i++)
		output->row_height[i] = row_height[i];

	scaling_distance = offsetof(struct rkvdec_hevc_priv_tbl, scaling_list);
	scaling_list_address = dma_base_addr + scaling_distance;
	output->scaling_list_address = scaling_list_address;
}

static void assemble_hw_pps(struct rkvdec_ctx *ctx, struct rkvdec_hevc_run *run)
{
	struct rkvdec_hevc_ctx *hevc_ctx = ctx->priv;
	const struct v4l2_ctrl_hevc_pps *pps = run->pps;
	struct rkvdec_hevc_priv_tbl *priv_tbl = hevc_ctx->priv_tbl.cpu;
	struct rkvdec_sps_pps_packet *hw_ps;
	struct rkvdec_sps_pps_bit_field bit_field;

	/*
	 * The hardware finds the appropriate SPS/PPS information from the PPS packet
	 * by indexing it with the ID of the PPS.
	 * The offset from the base is calculated with PPS_id * 80 (which is the size
	 * per PPS packet unit).
	 */
	hw_ps = &priv_tbl->param_set[pps->pic_parameter_set_id];
	memset(hw_ps, 0, sizeof(*hw_ps));
	memset(&bit_field, 0, sizeof(struct rkvdec_sps_pps_bit_field));
	translate_pps(run, &bit_field, hevc_ctx->priv_tbl.dma);
	memcpy(hw_ps->info, &bit_field, sizeof(struct rkvdec_sps_pps_bit_field));

	if (debug_dump & DUMP_PPS)
		print_hex_dump(KERN_INFO, "pps: ", DUMP_PREFIX_OFFSET, 32, 4,
			       (__force const void *)hw_ps->info, sizeof(struct rkvdec_sps_pps_packet), false);
}

/*
 * Creation of the Reference Picture Set memory blob for the hardware.
 * The layout looks like this:
 * [0] 32 bits for L0 (6 references + 2 bits of the 7th reference)
 * [1] 32 bits for L0 (remaining 3 bits of the 7th reference + 5 references
 *     + 4 bits of the 13th reference)
 * [2] 11 bits for L0 (remaining bit for 13 and 2 references) and
 *     21 bits for L1 (4 references + first bit of 5)
 * [3] 32 bits of padding with 0s
 * [4] 32 bits for L1 (remaining 4 bits for 5 + 5 references + 3 bits of 11)
 * [5] 22 bits for L1 (remaining 2 bits of 11 and 4 references)
 *     lowdelay flag (bit 23), rps bit offset long term (bit 24 - 32)
 * [6] rps bit offset long term (bit 1 - 3),  rps bit offset short term (bit 4 - 12)
 *     number of references (bit 13 - 16), remaining 16 bits of padding with 0s
 * [7] 32 bits of padding with 0s
 *
 * Thus we have to set up padding in between reference 5 of the L1 list.
 */
static void assemble_hw_rps(struct rkvdec_ctx *ctx, struct rkvdec_hevc_run *run)
{
	const struct v4l2_ctrl_hevc_decode_params *decode_params = run->decode_params;
	const struct v4l2_ctrl_hevc_sps *sps = run->sps;
	const struct v4l2_ctrl_hevc_slice_params *sl_params;
	const struct v4l2_hevc_dpb_entry *dpb;
	struct rkvdec_hevc_ctx *hevc_ctx = ctx->priv;
	struct rkvdec_hevc_priv_tbl *priv_tbl = hevc_ctx->priv_tbl.cpu;
	struct rkvdec_rps_packet *hw_ps;
	struct bit_writer bw;
	int i, j;
	unsigned int lowdelay;

	for (j = 0; j < run->num_slices; j++) {
		uint st_bit_offset = 0;
		uint num_l0_refs = 0;
		uint num_l1_refs = 0;

		sl_params = &run->slices_params[j];
		dpb = decode_params->dpb;

		if (sl_params->slice_type != V4L2_HEVC_SLICE_TYPE_I) {
			num_l0_refs = sl_params->num_ref_idx_l0_active_minus1 + 1;

			if (sl_params->slice_type == V4L2_HEVC_SLICE_TYPE_B)
				num_l1_refs = sl_params->num_ref_idx_l1_active_minus1 + 1;

			lowdelay = 1;
		} else {
			lowdelay = 0;
		}

		hw_ps = &priv_tbl->rps[j];
		memset(hw_ps, 0, sizeof(*hw_ps));
		bit_writer_init(&bw, hw_ps->info, sizeof(struct rkvdec_rps_packet));

		for (i = 0; i < RKV_RPS_MAX_REFERENCES; i++) {
			if (i < num_l0_refs) {
				const struct v4l2_hevc_dpb_entry dpb_l0 = dpb[sl_params->ref_idx_l0[i]];

				bit_writer_put_bits(&bw, !!(dpb_l0.flags & V4L2_HEVC_DPB_ENTRY_LONG_TERM_REFERENCE), 1);
				bit_writer_put_bits(&bw, sl_params->ref_idx_l0[i], 4);

				if (dpb_l0.pic_order_cnt_val > sl_params->slice_pic_order_cnt)
					lowdelay = 0;
			} else {
				bit_writer_put_bits(&bw, 0, 5);
			}
		}

		for (i = 0; i < RKV_RPS_MAX_REFERENCES; i++) {
			if (i < num_l1_refs) {
				const struct v4l2_hevc_dpb_entry dpb_l1 = dpb[sl_params->ref_idx_l1[i]];
				int is_long_term = !!(dpb_l1.flags & V4L2_HEVC_DPB_ENTRY_LONG_TERM_REFERENCE);

				bit_writer_put_bits(&bw, is_long_term, 1);
				if (i == 4)
					bit_writer_put_bits(&bw, 0, 32);
				bit_writer_put_bits(&bw, sl_params->ref_idx_l1[i], 4);

				if (dpb_l1.pic_order_cnt_val > sl_params->slice_pic_order_cnt)
					lowdelay = 0;
			} else {
				if (i == 4) {
					/* long term bit + 32 bits padding + 4 bits for the DPB index */
					bit_writer_put_bits(&bw, 0, 37);
				} else {
					bit_writer_put_bits(&bw, 0, 5);
				}
			}
		}
		bit_writer_put_bits(&bw, lowdelay, 1);

		if (!(decode_params->flags & V4L2_HEVC_DECODE_PARAM_FLAG_IDR_PIC)) {
			if (sl_params->short_term_ref_pic_set_size)
				st_bit_offset = sl_params->short_term_ref_pic_set_size;
			else if (sps->num_short_term_ref_pic_sets > 1)
				st_bit_offset = fls(sps->num_short_term_ref_pic_sets - 1);
		}

		bit_writer_put_bits(&bw, st_bit_offset + sl_params->long_term_ref_pic_set_size, 10);
		bit_writer_put_bits(&bw, st_bit_offset, 9);

		bit_writer_put_bits(&bw, decode_params->num_poc_st_curr_before + decode_params->num_poc_st_curr_after + decode_params->num_poc_lt_curr, 4);

		if (debug_dump & DUMP_RPS)
			print_hex_dump(KERN_INFO, "rps: ", DUMP_PREFIX_OFFSET, 32, 4,
				       (__force const void *)hw_ps->info, sizeof(struct rkvdec_rps_packet), false);
	}
}

/*
 * Flip one or more matrices along their main diagonal and flatten them
 * before writing it to the memory.
 * Convert:
 * ABCD         AEIM
 * EFGH     =>  BFJN     =>     AEIMBFJNCGKODHLP
 * IJKL         CGKO
 * MNOP         DHLP
 */
static void rotate_and_flatten_matrices(u8* output, const u8* input, int matrices, int row_length)
{
	int i, j, row, x_offset, matrix_offset, rot_index, y_offset, matrix_size, new_value;
	matrix_size = row_length * row_length;
	for (i = 0; i < matrices; i++) {
		row = 0;
		x_offset = 0;
		matrix_offset = i * matrix_size;
		for (j = 0; j < matrix_size; j++) {
			y_offset = j - (row * row_length);
			rot_index = y_offset * row_length + x_offset;
			new_value = *(input + i * matrix_size + j);
			output[matrix_offset + rot_index] = *(input + i * matrix_size + j);
			if ((j + 1) % row_length == 0) {
				row += 1;
				x_offset += 1;
			}
		}
	}
}

static void assemble_scalingfactor0(u8* output, const struct v4l2_ctrl_hevc_scaling_matrix *input)
{
	int mem_offset = 0;
	rotate_and_flatten_matrices(output, (const u8*)input->scaling_list_4x4, 6, 4);
	mem_offset = 6 * 16 * sizeof(u8);
	rotate_and_flatten_matrices(output + mem_offset, (const u8*)input->scaling_list_8x8, 6, 8);
	mem_offset += 6 * 64 * sizeof(u8);
	rotate_and_flatten_matrices(output + mem_offset, (const u8*)input->scaling_list_16x16, 6, 8);
	mem_offset += 6 * 64 * sizeof(u8);
	/* Add a 128 byte padding with 0s between the two 32x32 matrices */
	rotate_and_flatten_matrices(output + mem_offset, (const u8*)input->scaling_list_32x32, 1, 8);
	mem_offset += 64 * sizeof(u8);
	memset(output + mem_offset, 0, 128);
	mem_offset += 128 * sizeof(u8);
	rotate_and_flatten_matrices(output + mem_offset, (const u8*)input->scaling_list_32x32 + (64 * sizeof(u8)), 1, 8);
	mem_offset += 64 * sizeof(u8);
	memset(output + mem_offset, 0, 128);
}

/*
 * Required layout:
 * A = scaling_list_dc_coef_16x16
 * B = scaling_list_dc_coef_32x32
 * 0 = Padding
 *
 * A, A, A, A, A, A, B, 0, 0, B, 0, 0
 */
static void assemble_scalingdc(u8* output, const struct v4l2_ctrl_hevc_scaling_matrix *input)
{
	u8 list_32x32[6] = {0};
	memcpy(output, input->scaling_list_dc_coef_16x16, 6 * sizeof(u8));
	list_32x32[0] = input->scaling_list_dc_coef_32x32[0];
	list_32x32[3] = input->scaling_list_dc_coef_32x32[1];
	memcpy(output + 6 * sizeof(u8), list_32x32, 6 * sizeof(u8));
}

static void translate_scaling_list(struct scaling_factor *output, const struct v4l2_ctrl_hevc_scaling_matrix *input)
{
	assemble_scalingfactor0(output->scalingfactor0, input);
	memcpy(output->scalingfactor1, (const u8*)input->scaling_list_4x4, 96);
	assemble_scalingdc(output->scalingdc, input);
	memset(output->reserverd, 0, 4 * sizeof(u8));
}

static void assemble_hw_scaling_list(struct rkvdec_ctx *ctx, struct rkvdec_hevc_run *run)
{
	const struct v4l2_ctrl_hevc_scaling_matrix *scaling = run->scaling_matrix;
	struct rkvdec_hevc_ctx *hevc_ctx = ctx->priv;
	struct rkvdec_hevc_priv_tbl *tbl = hevc_ctx->priv_tbl.cpu;
	u8 *dst;

	if (!memcmp((void *)&hevc_ctx->scaling_matrix_cache, scaling,
		    sizeof(struct v4l2_ctrl_hevc_scaling_matrix)))
		return;

	dst = tbl->scaling_list;
	translate_scaling_list((struct scaling_factor *)dst, scaling);

	if (debug_dump & DUMP_SL)
		print_hex_dump(KERN_INFO, "scaling list: ", DUMP_PREFIX_OFFSET, 32, 4,
			       (__force const void *)dst, sizeof(struct scaling_factor), false);

	memcpy((void *)&hevc_ctx->scaling_matrix_cache, scaling,
	       sizeof(struct v4l2_ctrl_hevc_scaling_matrix));
}

static struct vb2_buffer *
get_ref_buf(struct rkvdec_ctx *ctx, struct rkvdec_hevc_run *run, unsigned int dpb_idx)
{
	struct v4l2_m2m_ctx *m2m_ctx = ctx->fh.m2m_ctx;
	const struct v4l2_ctrl_hevc_decode_params *decode_params = run->decode_params;
	const struct v4l2_hevc_dpb_entry *dpb = decode_params->dpb;
	struct vb2_queue *cap_q = &m2m_ctx->cap_q_ctx.q;
	int buf_idx = -1;

	if (dpb_idx < decode_params->num_active_dpb_entries)
		buf_idx = vb2_find_timestamp(cap_q, dpb[dpb_idx].timestamp, 0);

	/*
	 * If a DPB entry is unused or invalid, address of current destination
	 * buffer is returned.
	 */
	if (buf_idx < 0)
		return &run->base.bufs.dst->vb2_buf;

	return vb2_get_buffer(cap_q, buf_idx);
}

static void config_registers(struct rkvdec_ctx *ctx, struct rkvdec_hevc_run *run)
{
	struct rkvdec_dev *rkvdec = ctx->dev;
	const struct v4l2_ctrl_hevc_decode_params *decode_params = run->decode_params;
	const struct v4l2_ctrl_hevc_slice_params *sl_params = &run->slices_params[0];
	const struct v4l2_hevc_dpb_entry *dpb = decode_params->dpb;
	struct rkvdec_hevc_ctx *hevc_ctx = ctx->priv;
	dma_addr_t priv_start_addr = hevc_ctx->priv_tbl.dma;
	const struct v4l2_pix_format_mplane *dst_fmt;
	struct vb2_v4l2_buffer *src_buf = run->base.bufs.src;
	struct vb2_v4l2_buffer *dst_buf = run->base.bufs.dst;
	const struct v4l2_format *f;
	dma_addr_t rlc_addr;
	dma_addr_t refer_addr;
	u32 rlc_len;
	u32 hor_virstride;
	u32 ver_virstride;
	u32 y_virstride;
	u32 uv_virstride;
	u32 yuv_virstride;
	u32 offset;
	dma_addr_t dst_addr;
	u32 reg, i;

	reg = RKVDEC_MODE(RKVDEC_MODE_HEVC);
	writel_relaxed(reg, rkvdec->regs + RKVDEC_REG_SYSCTRL);

	f = &ctx->decoded_fmt;
	dst_fmt = &f->fmt.pix_mp;
	hor_virstride = dst_fmt->plane_fmt[0].bytesperline;
	ver_virstride = dst_fmt->height;
	y_virstride = hor_virstride * ver_virstride;
	uv_virstride = y_virstride / 2;
	yuv_virstride = y_virstride + uv_virstride;

	reg = RKVDEC_Y_HOR_VIRSTRIDE(hor_virstride / 16) |
		RKVDEC_UV_HOR_VIRSTRIDE(hor_virstride / 16) |
		RKVDEC_SLICE_NUM_LOWBITS(run->num_slices);
	writel_relaxed(reg, rkvdec->regs + RKVDEC_REG_PICPAR);

	/* config rlc base address */
	rlc_addr = vb2_dma_contig_plane_dma_addr(&src_buf->vb2_buf, 0);
	writel_relaxed(rlc_addr, rkvdec->regs + RKVDEC_REG_STRM_RLC_BASE);

	rlc_len = vb2_get_plane_payload(&src_buf->vb2_buf, 0);
	reg = RKVDEC_STRM_LEN(round_up(rlc_len, 16) + 64);
	writel_relaxed(reg, rkvdec->regs + RKVDEC_REG_STRM_LEN);

	/* config cabac table */
	offset = offsetof(struct rkvdec_hevc_priv_tbl, cabac_table);
	writel_relaxed(priv_start_addr + offset, rkvdec->regs + RKVDEC_REG_CABACTBL_PROB_BASE);

	/* config output base address */
	dst_addr = vb2_dma_contig_plane_dma_addr(&dst_buf->vb2_buf, 0);
	writel_relaxed(dst_addr, rkvdec->regs + RKVDEC_REG_DECOUT_BASE);

	reg = RKVDEC_Y_VIRSTRIDE(y_virstride / 16);
	writel_relaxed(reg, rkvdec->regs + RKVDEC_REG_Y_VIRSTRIDE);

	reg = RKVDEC_YUV_VIRSTRIDE(yuv_virstride / 16);
	writel_relaxed(reg, rkvdec->regs + RKVDEC_REG_YUV_VIRSTRIDE);

	/* config ref pic address */
	for (i = 0; i < 15; i++) {
		struct vb2_buffer *vb_buf = get_ref_buf(ctx, run, i);

		if (i < 4 && decode_params->num_active_dpb_entries) {
			reg = GENMASK(decode_params->num_active_dpb_entries - 1, 0);
			reg = (reg >> (i * 4)) & 0xf;
		} else {
			reg = 0;
		}

		refer_addr = vb2_dma_contig_plane_dma_addr(vb_buf, 0);
		writel_relaxed(refer_addr | reg, rkvdec->regs + RKVDEC_REG_H264_BASE_REFER(i));

		reg = RKVDEC_POC_REFER(i < decode_params->num_active_dpb_entries ? dpb[i].pic_order_cnt_val : 0);
		writel_relaxed(reg, rkvdec->regs + RKVDEC_REG_H264_POC_REFER0(i));
	}

	reg = RKVDEC_CUR_POC(sl_params->slice_pic_order_cnt);
	writel_relaxed(reg, rkvdec->regs + RKVDEC_REG_CUR_POC0);

	/* config hw pps address */
	offset = offsetof(struct rkvdec_hevc_priv_tbl, param_set);
	writel_relaxed(priv_start_addr + offset, rkvdec->regs + RKVDEC_REG_PPS_BASE);

	/* config hw rps address */
	offset = offsetof(struct rkvdec_hevc_priv_tbl, rps);
	writel_relaxed(priv_start_addr + offset, rkvdec->regs + RKVDEC_REG_RPS_BASE);

	reg = RKVDEC_AXI_DDR_RDATA(0);
	writel_relaxed(reg, rkvdec->regs + RKVDEC_REG_AXI_DDR_RDATA);

	reg = RKVDEC_AXI_DDR_WDATA(0);
	writel_relaxed(reg, rkvdec->regs + RKVDEC_REG_AXI_DDR_WDATA);
}

static int rkvdec_hevc_adjust_fmt(struct rkvdec_ctx *ctx, struct v4l2_format *f)
{
	struct v4l2_pix_format_mplane *fmt = &f->fmt.pix_mp;

	fmt->num_planes = 1;
	if (!fmt->plane_fmt[0].sizeimage)
		fmt->plane_fmt[0].sizeimage =
			fmt->width * fmt->height * RKV_MAX_DEPTH_IN_BYTES;
	return 0;
}

static int rkvdec_hevc_start(struct rkvdec_ctx *ctx)
{
	struct rkvdec_dev *rkvdec = ctx->dev;
	struct rkvdec_hevc_priv_tbl *priv_tbl;
	struct rkvdec_hevc_ctx *hevc_ctx;
	int ret;

	hevc_ctx = kzalloc(sizeof(*hevc_ctx), GFP_KERNEL);
	if (!hevc_ctx)
		return -ENOMEM;

	priv_tbl = dma_alloc_coherent(rkvdec->dev, sizeof(*priv_tbl),
				      &hevc_ctx->priv_tbl.dma, GFP_KERNEL);
	if (!priv_tbl) {
		ret = -ENOMEM;
		goto err_free_ctx;
	}

	hevc_ctx->priv_tbl.size = sizeof(*priv_tbl);
	hevc_ctx->priv_tbl.cpu = priv_tbl;
	memset(priv_tbl, 0, sizeof(*priv_tbl));
	memcpy(priv_tbl->cabac_table, rkvdec_hevc_cabac_table, sizeof(rkvdec_hevc_cabac_table));

	ctx->priv = hevc_ctx;
	return 0;

err_free_ctx:
	kfree(hevc_ctx);
	return ret;
}

static void rkvdec_hevc_stop(struct rkvdec_ctx *ctx)
{
	struct rkvdec_hevc_ctx *hevc_ctx = ctx->priv;
	struct rkvdec_dev *rkvdec = ctx->dev;

	dma_free_coherent(rkvdec->dev, hevc_ctx->priv_tbl.size,
			  hevc_ctx->priv_tbl.cpu, hevc_ctx->priv_tbl.dma);
	kfree(hevc_ctx);
}

static int rkvdec_hevc_run_preamble(struct rkvdec_ctx *ctx, struct rkvdec_hevc_run *run)
{
	struct v4l2_ctrl *ctrl;

	ctrl = v4l2_ctrl_find(&ctx->ctrl_hdl, V4L2_CID_STATELESS_HEVC_DECODE_PARAMS);
	run->decode_params = ctrl ? ctrl->p_cur.p : NULL;
	if (WARN_ON(!run->decode_params))
		return -EINVAL;
	ctrl = v4l2_ctrl_find(&ctx->ctrl_hdl, V4L2_CID_STATELESS_HEVC_SLICE_PARAMS);
	run->slices_params = ctrl ? ctrl->p_cur.p : NULL;
	if (WARN_ON(!run->slices_params))
		return -EINVAL;
	run->num_slices = ctrl->new_elems;
	ctrl = v4l2_ctrl_find(&ctx->ctrl_hdl, V4L2_CID_STATELESS_HEVC_SPS);
	run->sps = ctrl ? ctrl->p_cur.p : NULL;
	if (WARN_ON(!run->sps))
		return -EINVAL;
	ctrl = v4l2_ctrl_find(&ctx->ctrl_hdl, V4L2_CID_STATELESS_HEVC_PPS);
	run->pps = ctrl ? ctrl->p_cur.p : NULL;
	if (WARN_ON(!run->pps))
		return -EINVAL;
	ctrl = v4l2_ctrl_find(&ctx->ctrl_hdl, V4L2_CID_STATELESS_HEVC_SCALING_MATRIX);
	run->scaling_matrix = ctrl ? ctrl->p_cur.p : NULL;
	if (WARN_ON(!run->scaling_matrix))
		return -EINVAL;

	rkvdec_run_preamble(ctx, &run->base);
	return 0;
}

static void start_hardware(struct rkvdec_dev *rkvdec)
{
	void __iomem *register_base_addr = rkvdec->regs;
	struct delayed_work *watchdog_worker = &rkvdec->watchdog_work;
	/*
	 * RkVDEC error detection doesn't work really well and seems racy
	 * and unstable. Disable it.
	 */
	writel_relaxed(0, register_base_addr + RKVDEC_REG_STRMD_ERR_EN);
	writel_relaxed(0, register_base_addr + RKVDEC_REG_H264_ERR_E);

	schedule_delayed_work(watchdog_worker, msecs_to_jiffies(2000));

	writel(1, register_base_addr + RKVDEC_REG_PREF_LUMA_CACHE_COMMAND);
	writel(1, register_base_addr + RKVDEC_REG_PREF_CHR_CACHE_COMMAND);

	if (debug_dump & DUMP_REGISTER)
		dump_registers(rkvdec);

	/* Start decoding! */
	writel(RKVDEC_INTERRUPT_DEC_E | RKVDEC_CONFIG_DEC_CLK_GATE_E |
		   RKVDEC_TIMEOUT_E | RKVDEC_BUF_EMPTY_E,
		   register_base_addr + RKVDEC_REG_INTERRUPT);
}

static int rkvdec_hevc_run(struct rkvdec_ctx *ctx)
{
	struct rkvdec_dev *rkvdec = ctx->dev;
	struct rkvdec_hevc_run run;
	int ret;

	ret = rkvdec_hevc_run_preamble(ctx, &run);
	if (ret) {
		pr_err("Missing required controls for the RkVDEC HEVC decoder.");
		return ret;
	}

	assemble_hw_scaling_list(ctx, &run);
	assemble_hw_pps(ctx, &run);
	assemble_hw_rps(ctx, &run);
	config_registers(ctx, &run);
	rkvdec_run_postamble(ctx, &run.base);
	start_hardware(rkvdec);

	return 0;
}

static int rkvdec_hevc_try_ctrl(struct rkvdec_ctx *ctx, struct v4l2_ctrl *ctrl)
{
	const struct v4l2_ctrl_hevc_sps *sps = ctrl->p_new.p_hevc_sps;

	if (sps->chroma_format_idc > 1)
		/* Only 4:0:0 and 4:2:0 are supported */
		return -EINVAL;
	if (sps->bit_depth_luma_minus8 != sps->bit_depth_chroma_minus8)
		/* Luma and chroma bit depth mismatch */
		return -EINVAL;
	if (sps->bit_depth_luma_minus8 != 0 && sps->bit_depth_luma_minus8 != 2)
		/* Only 8-bit and 10-bit is supported */
		return -EINVAL;
	return 0;
}

const struct rkvdec_coded_fmt_ops rkvdec_hevc_fmt_ops = {
	.adjust_fmt = rkvdec_hevc_adjust_fmt,
	.start = rkvdec_hevc_start,
	.stop = rkvdec_hevc_stop,
	.run = rkvdec_hevc_run,
	.try_ctrl = rkvdec_hevc_try_ctrl,
};
